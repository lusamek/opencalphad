# OpenCalphad

(OpenCalphad Mirror Repository)


Link to OC, GUI: 
https://github.com/lusamek/OpenCalphad



![](https://raw.githubusercontent.com/lusamek/OpenCalphad/master/OC-Screen1.jpg)



![](https://raw.githubusercontent.com/lusamek/OpenCalphad/master/Example-OC6-Fe-C-map3.jpg)




# Virtual Machine

Virtualmachine for VMPLAYER (Vmware):  
https://gitlab.com/lusamek/opencalphad/-/raw/main/vm/vm-vmdk-apps-thermo-oc-0.32-vm1-1686909004.zip


QEmu or Virtualbox: 
VMDK Datei (harddisk):  https://gitlab.com/lusamek/opencalphad/-/raw/main/version-3.2/debian-oc-v0.32.vmdk
https://gitlab.com/lusamek/opencalphad/-/raw/main/version-3.2/debian-oc-v0.32.vmdk



# Database

The database iron4 is recommended for ferrous alloys: 

https://gitlab.com/lusamek/opencalphad/-/blob/main/database/1725010512-2-iron4-04c-Bo-Sundman.tdb




# Examples 

OpenCalphad including Thermo-Calc examples ...

https://github.com/lusamek/OpenCalphad/tree/master/macros



## Comparison with Thermo-Calc, Influence of Mn

![](medias/Picture-Fe-C-5Mn-P1.png)


![](medias/Picture-Fe-C-5Mn-P2.png)


![](medias/Picture-Fe-C-5Mn-P3.png)


![](https://gitlab.com/lusamek/opencalphad/-/blob/main/My_Project_-_Fe-C-2.5Mn-2.5Si.png)


![](medias/Macro_OC_-_HowTo_-_Fe-C-2.5Mn-2.5Si.png)


# Presentation

https://github.com/lusamek/OpenCalphad/blob/master/Presentations/1674728199-1-Presentation-OpenCalphad-Mr.-Axel-Menager.pptx


# FeCMnSi

![](https://gitlab.com/lusamek/opencalphad/-/raw/main/medias/Macro_OC_-_HowTo_-_Fe-C-2.5Mn-2.5Si-sus-ent.png)

![](https://gitlab.com/lusamek/opencalphad/-/raw/main/medias/TC_-_Fe-C-2.5Mn-2.5Si-sus-ent.png)

# Links

https://github.com/lusamek/OpenCalphad



# Literature

https://gitlab.com/lusamek/opencalphad/-/raw/main/literature/Thesis-Ludovic-Samek__2002__USTL_Lille__material_sciences_.pdf


https://link.springer.com/article/10.1007/s00501-012-0076-x

https://www.kth.se/hero-m-2i/about-hero-m-2i/scientific-advisors-1.1055174






